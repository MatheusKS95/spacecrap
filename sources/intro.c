/*
Copyright (C) 2021  Matheus Klein Schaefer and Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "intro.h"

static void logic(void);
static void draw(void);

void initIntro(void)
{
    app.delegate.logic = logic;
    app.delegate.draw = draw;

    memset(app.keyboard, 0, sizeof(int) * MAX_KEYBOARD_KEYS);
}

static void logic(void)
{
    doBackground();

    if (app.keyboard[SDL_SCANCODE_RETURN])
    {
        initStage();
    }
}

static void draw(void)
{
    drawBackground();

    drawText(SCREEN_WIDTH / 2, 40, 255, 255, 255, TEXT_CENTER, "YOU ARE THE OWNER OF SPACECRAP, A SHIP KNOWN");
    drawText(SCREEN_WIDTH / 2, 80, 255, 255, 255, TEXT_CENTER, "FOR IT'S HIGHLY DUBIOUS SAFETY STANDARDS.");
    drawText(SCREEN_WIDTH / 2, 120, 255, 255, 255, TEXT_CENTER, "YOU MUST TAKE HER TO A INSPECTION, HOWEVER");
    drawText(SCREEN_WIDTH / 2, 160, 255, 255, 255, TEXT_CENTER, "ON THE WAY THERE ARE RUSSIAN PIRATES WHOSE LEADER");
    drawText(SCREEN_WIDTH / 2, 200, 255, 255, 255, TEXT_CENTER, "IS KNOWN AS BLYATMAN. THEY ARE HOSTILE");
    drawText(SCREEN_WIDTH / 2, 240, 255, 255, 255, TEXT_CENTER, "AND YOU MUST GET RID OF THEM ONCE AND FOR ALL.");

    drawText(SCREEN_WIDTH / 2, 400, 255, 255, 255, TEXT_CENTER, "ARROW KEYS - MOVE, SPACE - SHOOT, ENTER TO BEGIN");
}
