/*
Copyright (C) 2021  Matheus Klein Schaefer and Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "stage2.h"

static void logic_stage2(void);
static void draw_stage2(void);
static void initPlayer_stage2(void);
static void fireBullet_stage2(void);
static void doPlayer_stage2(void);
static void doFighters_stage2(void);
static void doBullets_stage2(void);
static void drawFighters_stage2(void);
static void drawBullets_stage2(void);
static void spawnEnemies_stage2(void);
static int bulletHitFighter_stage2(Entity *b);
static void doEnemies_stage2(void);
static void fireAlienBullet_stage2(Entity *e);
static void clipPlayer_stage2(void);
static void resetStage2(void);
static void drawExplosions_stage2(void);
static void doExplosions_stage2(void);
static void addExplosions_stage2(int x, int y, int num);
static void drawHud_stage2(void);
static Entity *player_stage2;
static SDL_Texture *bulletTexture_stage2;
static SDL_Texture *enemyTexture_stage2;
static SDL_Texture *alienBulletTexture_stage2;
static SDL_Texture *playerTexture_stage2;
static SDL_Texture *explosionTexture_stage2;
static int enemySpawnTimer_stage2;
static int stageResetTimer_stage2;

void initStage2(int score)
{
    app.delegate.logic = logic_stage2;
    app.delegate.draw = draw_stage2;

    bulletTexture_stage2 = loadTexture("../multimedia/bullet.png");
    enemyTexture_stage2 = loadTexture("../multimedia/blyatman.png");
    alienBulletTexture_stage2 = loadTexture("../multimedia/blyatmanbullet.png");
    playerTexture_stage2 = loadTexture("../multimedia/spacecrap.png");
    explosionTexture_stage2 = loadTexture("../multimedia/explosion.png");

    memset(app.keyboard, 0, sizeof(int) * MAX_KEYBOARD_KEYS);

    loadMusic("../multimedia/soviet_anthem.oga");
    playMusic(1);

    resetStage2();

    stage2.score = score;

    initPlayer_stage2();

    enemySpawnTimer_stage2 = 0;

    stageResetTimer_stage2 = FPS * 3;
}

static void resetStage2(void)
{
    Entity *e;
    Explosion *ex;

    while (stage2.fighterHead.next)
    {
        e = stage2.fighterHead.next;
        stage2.fighterHead.next = e->next;
        free(e);
    }

    while (stage2.bulletHead.next)
    {
        e = stage2.bulletHead.next;
        stage2.bulletHead.next = e->next;
        free(e);
    }

    while (stage2.explosionHead.next)
    {
        ex = stage2.explosionHead.next;
        stage2.explosionHead.next = ex->next;
        free(ex);
    }

    memset(&stage2, 0, sizeof(Stage));
    stage2.fighterTail = &stage2.fighterHead;
    stage2.bulletTail = &stage2.bulletHead;
    stage2.explosionTail = &stage2.explosionHead;
}

static void initPlayer_stage2()
{
    player_stage2 = malloc(sizeof(Entity));
    memset(player_stage2, 0, sizeof(Entity));
    stage2.fighterTail->next = player_stage2;
    stage2.fighterTail = player_stage2;

    player_stage2->health = 10;
    player_stage2->x = SCREEN_WIDTH / 2;
    player_stage2->y = 500;
    player_stage2->texture = playerTexture_stage2;
    SDL_QueryTexture(player_stage2->texture, NULL, NULL, &player_stage2->w, &player_stage2->h);

    player_stage2->side = SIDE_PLAYER;
}

static void logic_stage2(void)
{
    doBackground();

    doPlayer_stage2();

    doEnemies_stage2();

    doFighters_stage2();

    doBullets_stage2();

    doExplosions_stage2();

    spawnEnemies_stage2();

    clipPlayer_stage2();

    if(stage2.score >= 999)
    {
        loadMusic("../multimedia/fight.ogg");
        playMusic(1);
        initFinish(stage2.score);
    }

    if (player_stage2 == NULL && --stageResetTimer_stage2 <= 0)
    {
        loadMusic("../multimedia/fight.ogg");
        playMusic(1);
        addHighscore(stage2.score);

        initHighscores();
    }
}

static void doPlayer_stage2(void)
{
    if (player_stage2 != NULL)
    {
        player_stage2->dx = player_stage2->dy = 0;

        if (player_stage2->reload > 0)
        {
            player_stage2->reload--;
        }

        if (app.keyboard[SDL_SCANCODE_UP])
        {
            player_stage2->dy = -PLAYER_SPEED;
        }

        if (app.keyboard[SDL_SCANCODE_DOWN])
        {
            player_stage2->dy = PLAYER_SPEED;
        }

        if (app.keyboard[SDL_SCANCODE_LEFT])
        {
            player_stage2->dx = -PLAYER_SPEED;
        }

        if (app.keyboard[SDL_SCANCODE_RIGHT])
        {
            player_stage2->dx = PLAYER_SPEED;
        }

        if (app.keyboard[SDL_SCANCODE_SPACE] && player_stage2->reload <= 0)
        {
            playSound(SND_PLAYER_FIRE, CH_PLAYER);

            fireBullet_stage2();
        }
    }
}

static void fireBullet_stage2(void)
{
    Entity *bullet;

    bullet = malloc(sizeof(Entity));
    memset(bullet, 0, sizeof(Entity));
    stage2.bulletTail->next = bullet;
    stage2.bulletTail = bullet;

    bullet->x = player_stage2->x + 10;
    bullet->y = player_stage2->y;
    bullet->dy = PLAYER_BULLET_SPEED;
    bullet->health = 1;
    bullet->texture = bulletTexture_stage2;
    bullet->side = player_stage2->side;
    SDL_QueryTexture(bullet->texture, NULL, NULL, &bullet->w, &bullet->h);

    bullet->y += (player_stage2->h / 2) - (bullet->h / 2);

    bullet->side = SIDE_PLAYER;

    player_stage2->reload = 16;
}

static void doEnemies_stage2(void)
{
    Entity *e;

    for (e = stage2.fighterHead.next ; e != NULL ; e = e->next)
    {
        if (e != player_stage2)
        {
            e->x = MIN(MAX(e->x, 0), SCREEN_WIDTH - e->h);

            if (player_stage2 != NULL && --e->reload <= 0)
            {
                fireAlienBullet_stage2(e);

                playSound(SND_ALIEN_FIRE, CH_ALIEN_FIRE);
            }
        }
    }
}

static void fireAlienBullet_stage2(Entity *e)
{
    Entity *bullet;

    bullet = malloc(sizeof(Entity));
    memset(bullet, 0, sizeof(Entity));
    stage2.bulletTail->next = bullet;
    stage2.bulletTail = bullet;

    bullet->x = e->x;
    bullet->y = e->y;
    bullet->health = 1;
    bullet->texture = alienBulletTexture_stage2;
    bullet->side = e->side;
    SDL_QueryTexture(bullet->texture, NULL, NULL, &bullet->w, &bullet->h);

    bullet->x += (e->w / 2) - (bullet->w / 2);
    bullet->y += (e->h / 2) - (bullet->h / 2);

    calcSlope(player_stage2->x + (player_stage2->w / 2), player_stage2->y + (player_stage2->h / 2), e->x, e->y, &bullet->dx, &bullet->dy);

    bullet->dx *= MINION_BULLET_SPEED;
    bullet->dy *= MINION_BULLET_SPEED;

    bullet->side = SIDE_ALIEN;

    e->reload = 5;
}

static void doFighters_stage2(void)
{
    Entity *e, *prev;

    prev = &stage2.fighterHead;

    for (e = stage2.fighterHead.next ; e != NULL ; e = e->next)
    {
        e->x += e->dx;
        e->y += e->dy;

        if (e != player_stage2 && e->x < -e->w)
        {
            e->health = 0;
        }

        if (e->health == 0)
        {
            if (e == player_stage2)
            {
                player_stage2 = NULL;
            }

            if (e == stage2.fighterTail)
            {
                stage2.fighterTail = prev;
                stage2.score++;
            }

            prev->next = e->next;
            free(e);
            e = prev;
        }

        prev = e;
    }
}

static void doBullets_stage2(void)
{
    Entity *b, *prev;

    prev = &stage2.bulletHead;

    for (b = stage2.bulletHead.next ; b != NULL ; b = b->next)
    {
        b->x += b->dx;
        b->y += b->dy;

        if (bulletHitFighter_stage2(b) || b->x < -b->w || b->y < -b->h || b->x > SCREEN_WIDTH || b->y > SCREEN_HEIGHT)
        {
            if (b == stage2.bulletTail)
            {
                stage2.bulletTail = prev;
            }

            prev->next = b->next;
            free(b);
            b = prev;
        }

        prev = b;
    }
}

static int bulletHitFighter_stage2(Entity *b)
{
    Entity *e;

    for (e = stage2.fighterHead.next ; e != NULL ; e = e->next)
    {
        if (e->side != b->side && collision(b->x, b->y, b->w, b->h, e->x, e->y, e->w, e->h))
        {
            b->health = 0;
            e->health--;

            addExplosions_stage2(e->x, e->y, 32);

            if (e == player_stage2)
            {
                playSound(SND_PLAYER_DIE, CH_PLAYER);
            }
            else
            {
                playSound(SND_ALIEN_DIE, CH_ANY);
            }

            return 1;
        }
    }

    return 0;
}

static void spawnEnemies_stage2(void)
{
    Entity *enemy;

    if (--enemySpawnTimer_stage2 <= 0)
    {
        enemy = malloc(sizeof(Entity));
        memset(enemy, 0, sizeof(Entity));
        stage2.fighterTail->next = enemy;
        stage2.fighterTail = enemy;

        enemy->x = rand() % SCREEN_WIDTH;
        enemy->y = 0;
        enemy->texture = enemyTexture_stage2;
        SDL_QueryTexture(enemy->texture, NULL, NULL, &enemy->w, &enemy->h);

        enemy->dy = 1;
        enemy->dx = -100 + (rand() % 200) / 2;
        enemy->dx /= 100;

        enemy->side = SIDE_ALIEN;
        enemy->health = 5;

        enemy->reload = FPS * (1 + (rand() % 3));

        enemySpawnTimer_stage2 = 300 + (rand() % FPS);
    }
}

static void clipPlayer_stage2(void)
{
    if (player_stage2 != NULL)
    {
        if (player_stage2->x < 0)
        {
            player_stage2->x = 0;
        }

        if (player_stage2->y < 0)
        {
            player_stage2->y = 0;
        }

        if (player_stage2->x > SCREEN_WIDTH - player_stage2->w)
        {
            player_stage2->x = SCREEN_WIDTH - player_stage2->w;
        }

        if (player_stage2->y > SCREEN_HEIGHT - player_stage2->h)
        {
            player_stage2->y = SCREEN_HEIGHT - player_stage2->h;
        }
    }
}

static void doExplosions_stage2(void)
{
    Explosion *e, *prev;

    prev = &stage2.explosionHead;

    for (e = stage2.explosionHead.next ; e != NULL ; e = e->next)
    {
        e->x += e->dx;
        e->y += e->dy;

        if (--e->a <= 0)
        {
            if (e == stage2.explosionTail)
            {
                stage2.explosionTail = prev;
            }

            prev->next = e->next;
            free(e);
            e = prev;
        }

        prev = e;
    }
}

static void addExplosions_stage2(int x, int y, int num)
{
    Explosion *e;
    int i;

    for (i = 0 ; i < num ; i++)
    {
        e = malloc(sizeof(Explosion));
        memset(e, 0, sizeof(Explosion));
        stage2.explosionTail->next = e;
        stage2.explosionTail = e;

        e->x = x + (rand() % 32) - (rand() % 32);
        e->y = y + (rand() % 32) - (rand() % 32);
        e->dx = (rand() % 10) - (rand() % 10);
        e->dy = (rand() % 10) - (rand() % 10);

        e->dx /= 10;
        e->dy /= 10;

        switch (rand() % 4)
        {
            case 0:
                e->r = 255;
                break;

            case 1:
                e->r = 255;
                e->g = 128;
                break;

            case 2:
                e->r = 255;
                e->g = 255;
                break;

            default:
                e->r = 255;
                e->g = 255;
                e->b = 255;
                break;
        }

        e->a = rand() % FPS * 3;
    }
}

static void draw_stage2(void)
{
    drawBackground();

    drawFighters_stage2();

    drawExplosions_stage2();

    drawBullets_stage2();

    drawHud_stage2();
}

static void drawFighters_stage2(void)
{
    Entity *e;

    for (e = stage2.fighterHead.next ; e != NULL ; e = e->next)
    {
        blit(e->texture, e->x, e->y);
    }
}

static void drawBullets_stage2(void)
{
    Entity *b;

    for (b = stage2.bulletHead.next ; b != NULL ; b = b->next)
    {
        blit(b->texture, b->x, b->y);
    }
}

static void drawExplosions_stage2(void)
{
    Explosion *e;

    SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_ADD);
    SDL_SetTextureBlendMode(explosionTexture_stage2, SDL_BLENDMODE_ADD);

    for (e = stage2.explosionHead.next ; e != NULL ; e = e->next)
    {
        SDL_SetTextureColorMod(explosionTexture_stage2, e->r, e->g, e->b);
        SDL_SetTextureAlphaMod(explosionTexture_stage2, e->a);

        blit(explosionTexture_stage2, e->x, e->y);
    }

    SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
}

static void drawHud_stage2(void)
{
    drawText(10, 10, 255, 255, 255, TEXT_LEFT, "SCORE: %03d", stage2.score);

    if (stage2.score < highscores.highscore[0].score)
    {
        drawText(SCREEN_WIDTH - 10, 10, 255, 255, 255, TEXT_RIGHT, "BEST EVER: %03d", highscores.highscore[0].score);
    }
    else
    {
        drawText(SCREEN_WIDTH - 10, 10, 0, 255, 0, TEXT_RIGHT, "NEW BEST: %03d", stage2.score);
    }

    if(player_stage2 != NULL)
    {
        drawText(10, SCREEN_HEIGHT - 40, 255, 255, 155, TEXT_LEFT, "HEALTH: %d%%", player_stage2->health * 10);
    }
    else
    {
        drawText(10, SCREEN_HEIGHT - 40, 255, 255, 155, TEXT_LEFT, "HEALTH: 0%%");
    }
}
