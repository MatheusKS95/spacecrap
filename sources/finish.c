/*
Copyright (C) 2021  Matheus Klein Schaefer and Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "finish.h"

static void logic(void);
static void draw(void);
static int final_score;

void initFinish(int score)
{
    app.delegate.logic = logic;
    app.delegate.draw = draw;
    final_score = score;

    memset(app.keyboard, 0, sizeof(int) * MAX_KEYBOARD_KEYS);
}

static void logic(void)
{
    doBackground();

    if (app.keyboard[SDL_SCANCODE_RETURN])
    {
        addHighscore(final_score);

        initHighscores();
    }
}

static void draw(void)
{
    drawBackground();
    drawText(SCREEN_WIDTH / 2, 200, 255, 255, 255, TEXT_CENTER, "CONGRATULATIONS! YOU DEFEATED THE MINIONS!");
    drawText(SCREEN_WIDTH / 2, 240, 255, 255, 255, TEXT_CENTER, "BLYATMAN WILL RETURN");

    drawText(SCREEN_WIDTH / 2, 400, 255, 255, 255, TEXT_CENTER, "PRESS ENTER TO CONTINUE");
}
