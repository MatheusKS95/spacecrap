/*
Copyright (C) 2021  Matheus Klein Schaefer and Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "intermission.h"

static void logic(void);
static void draw(void);
static int score_transf;

void initIntermission(int score)
{
    app.delegate.logic = logic;
    app.delegate.draw = draw;
    score_transf = score;

    memset(app.keyboard, 0, sizeof(int) * MAX_KEYBOARD_KEYS);
}

static void logic(void)
{
    doBackground();

    if (app.keyboard[SDL_SCANCODE_RETURN])
    {
        initStage2(score_transf);
    }
}

static void draw(void)
{
    drawBackground();
    drawText(SCREEN_WIDTH / 2, 200, 255, 255, 255, TEXT_CENTER, "YOU ENRAGED BLYATMAN! BUT HE FLED.");
    drawText(SCREEN_WIDTH / 2, 240, 255, 255, 255, TEXT_CENTER, "HIS MOST LOYAL MINIONS WILL FIGHT FOR HIM!");
    drawText(SCREEN_WIDTH / 2, 280, 255, 255, 255, TEXT_CENTER, "BLYATMAN NEVER DO THINGS PERSONALLY...");

    drawText(SCREEN_WIDTH / 2, 400, 255, 255, 255, TEXT_CENTER, "PRESS ENTER TO CONTINUE");
}
