/*
Copyright (C) 2021  Matheus Klein Schaefer and Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "background.h"

static int backgroundY;
static SDL_Texture *background;

void initBackground(void)
{
    background = loadTexture("../multimedia/background.png");
	
    backgroundY = 0;
}

void doBackground(void)
{
    if (--backgroundY < -SCREEN_HEIGHT)
	{
        backgroundY = 0;
	}
}

void drawBackground(void)
{
    SDL_Rect dest;
	
    for (int y = backgroundY; y < SCREEN_HEIGHT; y += SCREEN_HEIGHT)
	{
        dest.x = 0;
        dest.y = -y;
		dest.w = SCREEN_WIDTH;
		dest.h = SCREEN_HEIGHT;
		
		SDL_RenderCopy(app.renderer, background, NULL, &dest);
	}
}
