/*
Copyright (C) 2021  Matheus Klein Schaefer and Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "stage.h"

static void logic(void);
static void draw(void);
static void initPlayer(void);
static void fireBullet(void);
static void doPlayer(void);
static void doFighters(void);
static void doBullets(void);
static void drawFighters(void);
static void drawBullets(void);
static void spawnEnemies(void);
static int bulletHitFighter(Entity *b);
static void doEnemies(void);
static void fireAlienBullet(Entity *e);
static void clipPlayer(void);
static void resetStage(void);
static void drawExplosions(void);
static void doExplosions(void);
static void addExplosions(int x, int y, int num);
static void drawHud(void);
static Entity *player;
static SDL_Texture *bulletTexture;
static SDL_Texture *enemyTexture;
static SDL_Texture *alienBulletTexture;
static SDL_Texture *playerTexture;
static SDL_Texture *explosionTexture;
static int enemySpawnTimer;
static int stageResetTimer;

void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;
	
    bulletTexture = loadTexture("../multimedia/bullet.png");
    enemyTexture = loadTexture("../multimedia/enemy.png");
    alienBulletTexture = loadTexture("../multimedia/enemybullet.png");
    playerTexture = loadTexture("../multimedia/spacecrap.png");
    explosionTexture = loadTexture("../multimedia/explosion.png");
	
	memset(app.keyboard, 0, sizeof(int) * MAX_KEYBOARD_KEYS);
	
	resetStage();
	
	stage.score = 0;
	
	initPlayer();
	
	enemySpawnTimer = 0;
	
	stageResetTimer = FPS * 3;
}

static void resetStage(void)
{
	Entity *e;
    Explosion *ex;
	
	while (stage.fighterHead.next)
	{
		e = stage.fighterHead.next;
		stage.fighterHead.next = e->next;
		free(e);
	}
	
	while (stage.bulletHead.next)
	{
		e = stage.bulletHead.next;
		stage.bulletHead.next = e->next;
		free(e);
	}
	
	while (stage.explosionHead.next)
	{
		ex = stage.explosionHead.next;
		stage.explosionHead.next = ex->next;
		free(ex);
    }
	
	memset(&stage, 0, sizeof(Stage));
	stage.fighterTail = &stage.fighterHead;
	stage.bulletTail = &stage.bulletHead;
    stage.explosionTail = &stage.explosionHead;
}

static void initPlayer()
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.fighterTail->next = player;
	stage.fighterTail = player;
	
    player->health = 10;
    player->x = SCREEN_WIDTH / 2;
    player->y = 500;
	player->texture = playerTexture;
	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
	
	player->side = SIDE_PLAYER;
}

static void logic(void)
{
    doBackground();
	
	doPlayer();
	
	doEnemies();
	
	doFighters();
	
	doBullets();
	
    doExplosions();
	
	spawnEnemies();
	
	clipPlayer();

    if(stage.score >= 800)
    {
        initIntermission(stage.score);
    }
	
	if (player == NULL && --stageResetTimer <= 0)
	{
		addHighscore(stage.score);
		
		initHighscores();
	}
}

static void doPlayer(void)
{
	if (player != NULL)
	{
		player->dx = player->dy = 0;
		
		if (player->reload > 0)
		{
			player->reload--;
		}
		
		if (app.keyboard[SDL_SCANCODE_UP])
		{
			player->dy = -PLAYER_SPEED;
		}
		
		if (app.keyboard[SDL_SCANCODE_DOWN])
		{
			player->dy = PLAYER_SPEED;
		}
		
		if (app.keyboard[SDL_SCANCODE_LEFT])
		{
			player->dx = -PLAYER_SPEED;
		}
		
		if (app.keyboard[SDL_SCANCODE_RIGHT])
		{
			player->dx = PLAYER_SPEED;
		}
		
        if (app.keyboard[SDL_SCANCODE_SPACE] && player->reload <= 0)
		{
			playSound(SND_PLAYER_FIRE, CH_PLAYER);
			
			fireBullet();
		}
	}
}

static void fireBullet(void)
{
	Entity *bullet;
	
	bullet = malloc(sizeof(Entity));
	memset(bullet, 0, sizeof(Entity));
	stage.bulletTail->next = bullet;
	stage.bulletTail = bullet;
	
    bullet->x = player->x + 10;
	bullet->y = player->y;
    bullet->dy = PLAYER_BULLET_SPEED;
	bullet->health = 1;
	bullet->texture = bulletTexture;
	bullet->side = player->side;
	SDL_QueryTexture(bullet->texture, NULL, NULL, &bullet->w, &bullet->h);
	
	bullet->y += (player->h / 2) - (bullet->h / 2);
	
	bullet->side = SIDE_PLAYER;
	
    player->reload = 16;
}

static void doEnemies(void)
{
	Entity *e;
	
	for (e = stage.fighterHead.next ; e != NULL ; e = e->next)
	{
		if (e != player)
		{
            e->x = MIN(MAX(e->x, 0), SCREEN_WIDTH - e->h);
			
			if (player != NULL && --e->reload <= 0)
			{
				fireAlienBullet(e);
				
				playSound(SND_ALIEN_FIRE, CH_ALIEN_FIRE);
			}
		}
	}
}

static void fireAlienBullet(Entity *e)
{
	Entity *bullet;
	
	bullet = malloc(sizeof(Entity));
	memset(bullet, 0, sizeof(Entity));
	stage.bulletTail->next = bullet;
	stage.bulletTail = bullet;
	
	bullet->x = e->x;
	bullet->y = e->y;
	bullet->health = 1;
	bullet->texture = alienBulletTexture;
	bullet->side = e->side;
	SDL_QueryTexture(bullet->texture, NULL, NULL, &bullet->w, &bullet->h);
	
	bullet->x += (e->w / 2) - (bullet->w / 2);
	bullet->y += (e->h / 2) - (bullet->h / 2);
	
	calcSlope(player->x + (player->w / 2), player->y + (player->h / 2), e->x, e->y, &bullet->dx, &bullet->dy);
	
	bullet->dx *= ALIEN_BULLET_SPEED;
	bullet->dy *= ALIEN_BULLET_SPEED;
	
	bullet->side = SIDE_ALIEN;
	
	e->reload = (rand() % FPS * 2);
}

static void doFighters(void)
{
	Entity *e, *prev;
	
	prev = &stage.fighterHead;
	
	for (e = stage.fighterHead.next ; e != NULL ; e = e->next)
	{
		e->x += e->dx;
		e->y += e->dy;
		
		if (e != player && e->x < -e->w)
		{
			e->health = 0;
		}
		
		if (e->health == 0)
		{
			if (e == player)
			{
				player = NULL;
			}
			
			if (e == stage.fighterTail)
			{
				stage.fighterTail = prev;
			}
			
			prev->next = e->next;
			free(e);
			e = prev;
		}
		
		prev = e;
	}
}
	
static void doBullets(void)
{
	Entity *b, *prev;
	
	prev = &stage.bulletHead;
	
	for (b = stage.bulletHead.next ; b != NULL ; b = b->next)
	{
		b->x += b->dx;
		b->y += b->dy;
		
		if (bulletHitFighter(b) || b->x < -b->w || b->y < -b->h || b->x > SCREEN_WIDTH || b->y > SCREEN_HEIGHT)
		{
			if (b == stage.bulletTail)
			{
				stage.bulletTail = prev;
			}
			
			prev->next = b->next;
			free(b);
			b = prev;
		}
		
		prev = b;
	}
}

static int bulletHitFighter(Entity *b)
{
	Entity *e;
	
	for (e = stage.fighterHead.next ; e != NULL ; e = e->next)
	{
		if (e->side != b->side && collision(b->x, b->y, b->w, b->h, e->x, e->y, e->w, e->h))
		{
            b->health = 0;
            e->health--;
			
			addExplosions(e->x, e->y, 32);

			if (e == player)
			{
				playSound(SND_PLAYER_DIE, CH_PLAYER);
			}
			else
            {
				playSound(SND_ALIEN_DIE, CH_ANY);
                stage.score += 5;
			}
			
			return 1;
		}
	}
	
	return 0;
}

static void spawnEnemies(void)
{
	Entity *enemy;
	
	if (--enemySpawnTimer <= 0)
	{
		enemy = malloc(sizeof(Entity));
		memset(enemy, 0, sizeof(Entity));
		stage.fighterTail->next = enemy;
		stage.fighterTail = enemy;
		
        enemy->x = rand() % SCREEN_WIDTH;
        enemy->y = 0;
		enemy->texture = enemyTexture;
		SDL_QueryTexture(enemy->texture, NULL, NULL, &enemy->w, &enemy->h);
		
        enemy->dy = (1 + (rand() % 2));
        enemy->dx = -100 + (rand() % 200);
        enemy->dx /= 100;
		
		enemy->side = SIDE_ALIEN;
		enemy->health = 1;
		
		enemy->reload = FPS * (1 + (rand() % 3));
		
		enemySpawnTimer = 30 + (rand() % FPS);
	}
}

static void clipPlayer(void)
{
	if (player != NULL)
	{
		if (player->x < 0)
		{
			player->x = 0;
		}
		
		if (player->y < 0)
		{
			player->y = 0;
		}
		
		if (player->x > SCREEN_WIDTH - player->w)
		{
			player->x = SCREEN_WIDTH - player->w;
		}
		
		if (player->y > SCREEN_HEIGHT - player->h)
		{
			player->y = SCREEN_HEIGHT - player->h;
		}
	}
}

static void doExplosions(void)
{
	Explosion *e, *prev;
	
	prev = &stage.explosionHead;
	
	for (e = stage.explosionHead.next ; e != NULL ; e = e->next)
	{
		e->x += e->dx;
		e->y += e->dy;
		
		if (--e->a <= 0)
		{
			if (e == stage.explosionTail)
			{
				stage.explosionTail = prev;
			}
			
			prev->next = e->next;
			free(e);
			e = prev;
		}
		
		prev = e;
	}
}

static void addExplosions(int x, int y, int num)
{
	Explosion *e;
	int i;
	
	for (i = 0 ; i < num ; i++)
	{
		e = malloc(sizeof(Explosion));
		memset(e, 0, sizeof(Explosion));
		stage.explosionTail->next = e;
		stage.explosionTail = e;
		
		e->x = x + (rand() % 32) - (rand() % 32);
		e->y = y + (rand() % 32) - (rand() % 32);
		e->dx = (rand() % 10) - (rand() % 10);
		e->dy = (rand() % 10) - (rand() % 10);
		
		e->dx /= 10;
		e->dy /= 10;
		
		switch (rand() % 4)
		{
			case 0:
				e->r = 255;
				break;
				
			case 1:
				e->r = 255;
				e->g = 128;
				break;
				
			case 2:
				e->r = 255;
				e->g = 255;
				break;
				
			default:
				e->r = 255;
				e->g = 255;
				e->b = 255;
				break;
		}
		
		e->a = rand() % FPS * 3;
	}
}

static void draw(void)
{
	drawBackground();

    drawFighters();
	
	drawExplosions();
	
	drawBullets();
	
	drawHud();
}

static void drawFighters(void)
{
	Entity *e;
	
	for (e = stage.fighterHead.next ; e != NULL ; e = e->next)
	{
		blit(e->texture, e->x, e->y);
	}
}

static void drawBullets(void)
{
	Entity *b;
	
	for (b = stage.bulletHead.next ; b != NULL ; b = b->next)
	{
		blit(b->texture, b->x, b->y);
	}
}

static void drawExplosions(void)
{
	Explosion *e;
	
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_ADD);
	SDL_SetTextureBlendMode(explosionTexture, SDL_BLENDMODE_ADD);
	
	for (e = stage.explosionHead.next ; e != NULL ; e = e->next)
	{
		SDL_SetTextureColorMod(explosionTexture, e->r, e->g, e->b);
		SDL_SetTextureAlphaMod(explosionTexture, e->a);
		
		blit(explosionTexture, e->x, e->y);
	}
	
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);
}

static void drawHud(void)
{
    drawText(10, 10, 255, 255, 255, TEXT_LEFT, "SCORE: %03d", stage.score);
	
	if (stage.score < highscores.highscore[0].score)
	{
        drawText(SCREEN_WIDTH - 10, 10, 255, 255, 255, TEXT_RIGHT, "BEST EVER: %03d", highscores.highscore[0].score);
	}
	else
	{
        drawText(SCREEN_WIDTH - 10, 10, 0, 255, 0, TEXT_RIGHT, "NEW BEST: %03d", stage.score);
	}

    if(player != NULL)
    {
        drawText(10, SCREEN_HEIGHT - 40, 255, 255, 155, TEXT_LEFT, "HEALTH: %d%%", player->health * 10);
    }
    else
    {
        drawText(10, SCREEN_HEIGHT - 40, 255, 255, 155, TEXT_LEFT, "HEALTH: 0%%");
    }
}
