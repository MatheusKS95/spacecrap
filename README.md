# SpaceCrap

A simple game made with SDL2. It's purpose is for myself to learn SDL2, using no engine or other game development framework. Also, to improve my C skills (and general and procedural programming skills). SpaceCrap is intended to use as little resources as possible.

### How to setup?
You'll need CMake, SDL2, SDL2_image, SDL2_mixer and SDL2_TTF installed in order to build and run this project. The CmakeLists file already have the settings tested for Linux machines (`mkdir build && cd build`, then `cmake ..` and finish it up with `make`), but extra work may be required to run on Windows, BSD and/or MacOS X.

### Disclaimer
This is a WIP project, created for my own learning purposes. Please keep this in mind. Avoid negative feedback or any kind of insult regarding code quality/organization/whatever.

### Original artwork
Music, font and sound effects have their own licenses and it's on multimedia folder. Explosion effect came from Parallel Realities series of tutorials. Ships and bullets are my own work and under Creative Commons (CC-BY).

Font by Zingot Games under CC-BY 4.0 (full license inside multimedia directory)

Sound effects by Michel Baradari under CC-BY 3.0

Main music by Ville Nousiainen (public domain)

Boss fight composed by Alexander Vasilyevich Alexandrov. Lyrics by Sergey Mikhalkov. Not subject by copyright according to article 1259 of Book IV of the Civil Code of the Russian Federation No. 230-FZ of December 18, 2006.  https://en.wikipedia.org/wiki/File:Gimn_Sovetskogo_Soyuza_(1977_Vocal).oga

